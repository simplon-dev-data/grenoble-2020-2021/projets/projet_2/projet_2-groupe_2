# Projet_2-Groupe_2

### Membres du groupe 2 : Anthony, Merouane, Arnaud

## Description du dépôt GitLab

Un fichier python `main_script.py`, à éxecuter pour obtenir les données souhaitées pour le traitement des besoins du projet.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deux fichiers python auxiliaires `aux(...)entire_data.py` et `aux(...)sql.py` à éxecuter  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;respectivement pour collecter plus de données & pour en enregistrer l'historique.  

Un fichier Jupyter Notebook `tuto_jupyter.ipynb` contenant des informations expliquant comment s'approprier les éléments du projet.  

Un fichier `docker-compose.yml` contenant la configuration du container docker pour postgresql & metabase.  

Un fichier `presentation.pdf` contenant le slide de presentation/restitution du projet.

Un fichier `.gitignore` pour empêcher l'upload vers GitLab des fichiers volumineux du projet.  

Un fichier `notes.txt` contenant des informations accéssoires diverses.

Ce fichier `README.md` contenant le descriptif du dépôt GitLab et listant les requêtes SQL principales retenues au cours du projet.


Create tables
- raw_data
```SQL
CREATE TABLE raw_data(
    "longitude" FLOAT,
    "lattitude" FLOAT,
    "nom_dept" TEXT,
    "nom_com" TEXT,
    "insee_com" INT,
    "nom_station" TEXT,
    "code_station" TEXT,
    "typologie" TEXT,
    "influence" TEXT,
    "nom_poll" TEXT,
    "polluant_court" TEXT,
    "id_poll_ue" INT,
    "valeur" FLOAT,
    "unite" TEXT,
    "metrique" TEXT,
    "date_debut" TIMESTAMP WITH TIME ZONE,
    "date_fin" TIMESTAMP WITH TIME ZONE,
    "validite" TEXT,
    "x_wgs84" FLOAT,
    "y_wgs84" FLOAT,
    "x_reglementaire" FLOAT,
    "y_reglementaire" FLOAT,
    "OBJECTID" INT
);
```
- departement
```SQL
CREATE TABLE departement (
    "id_departement" SERIAL,
    "nom_dept" TEXT,
    PRIMARY KEY ("id_departement")
);
```
- commune
```SQL
CREATE TABLE commune (
    "id_dept" INT, 
    "nom_com" TEXT,
    "insee_com" INT,
    PRIMARY KEY ("insee_com")
    FOREIGN KEY ("id_dept") REFERENCES departement ("id_departement")
);
```
- station
```SQL
CREATE TABLE station (
    "code_station" SERIAL,
    "id_com" INT,
    "nom_station " TEXT,
    "longitude" FLOAT,
    "lattitude" FLOAT,
    PRIMARY KEY ("code_station")
    FOREIGN KEY ("id_com") REFERENCES commune ("insee_com")
);
```
- releve
```SQL
CREATE TABLE releve (
    "id_releve" SERIAL,
    "id_pol_releve" INT, 
    "id_station" INT,
    "date_heure_releve" TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY ("id_releve")
    FOREIGN KEY ("id_pole_releve") REFERENCES polluant ("id_polluant"),
    FOREIGN KEY ("id_station") REFERENCES station ("code_station")
);
```
- polluant
```SQL
CREATE TABLE polluant (
    "id_polluant" SERIAL,
    "nom_poll" INT,
    PRIMARY KEY ("id_pollaunt")
);
```
Insert into tables data from raw_data
- departement
```SQL
INSERT INTO departement("nom_dept") SELECT DISTINCT("nom_dept") FROM raw_data;
```
- commune
```SQL
INSERT INTO commune ("nom_com", "insee_com", "id_dept") 
SELECT DISTINCT (raw_data.nom_com), raw_data.insee_com, departement.id_departement 
FROM raw_data 
JOIN departement 
ON departement.nom_dept = raw_data.nom_dept;
```
- station
```SQL
INSERT INTO station ("nom_station", "longitude", "lattitude", "code_station", "id_com") SELECT DISTINCT(raw_data.nom_station), raw_data.longitude, raw_data.lattitude, raw_data.code_station, commune.insee_com 
FROM raw_data 
JOIN commune 
ON commune.insee_com = raw_data.insee_com;
```
- releve
```SQL
INSERT INTO releve ("id_pol_releve", "id_station", "date_heure_releve") 
SELECT polluant.id_polluant, station.id_station, raw_data.date_debut
FROM raw_data
JOIN polluant
ON polluant.nom_poll = raw_data.nom_poll
JOIN station
ON  station.nom_station = raw_data.nom_station; 
```
- polluant
```SQL
INSERT INTO polluant ("nom_poll") 
SELECT DISTINCT(nom_poll)
FROM raw_data;
```

Sliced year chart
```SQL
SELECT CAST(releve.date_heure_releve AS DATE) as "Date",
    AVG(releve.valeur) AS "Valeur moyenne",
    polluant.nom_poll AS "Polluant",
    CASE WHEN CAST(releve.date_heure_releve AS DATE) < '2020-03-17' THEN 'Avant le premier confinement'
        WHEN ('2020-03-17' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-05-11') THEN 'Pendant le premier confinement'
        WHEN ('2020-05-11' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-10-30') THEN 'Entre les deux confinements'
        WHEN ('2020-10-30' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-12-15') THEN 'Pendant le deuxième confinement'
        WHEN '2020-12-15' <= CAST(releve.date_heure_releve AS DATE) THEN 'Après le deuxième confinement'
        ELSE 'UNSET'
    END AS "Période"
FROM releve
JOIN polluant
    ON releve.id_pol_releve = polluant.id_polluant
GROUP BY "Date", "Polluant"
ORDER BY "Date"
;
```
Per poll year chart
```SQL
SELECT releve.date_heure_releve AS days,
        AVG(releve.valeur) AS valeur_moyenne,
        polluant.nom_poll AS polluant
FROM releve
JOIN polluant
    ON releve.id_pol_releve = polluant.id_polluant
GROUP BY days, polluant
ORDER BY days, polluant
;
```
Average values per period NO2
```SQL
SELECT AVG(releve.valeur) AS "NO₂(µg/m³)",
        CASE
        WHEN CAST(releve.date_heure_releve AS DATE) < '2020-03-17' THEN '1 Avant le premier confinement'
        WHEN ('2020-03-17' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-05-11') THEN '2 Pendant le premier confinement'
        WHEN ('2020-05-11' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-10-30') THEN '3 Entre les deux confinements'
        WHEN ('2020-10-30' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-12-15') THEN '4 Pendant le deuxième confinement'
        WHEN '2020-12-15' <= CAST(releve.date_heure_releve AS DATE) THEN '5 Après le deuxième confinement'
        ELSE 'UNSET'
    END AS "Période"
FROM releve
JOIN polluant
    ON releve.id_pol_releve = polluant.id_polluant
JOIN station
    ON releve.id_station = station.id_station
JOIN commune
    ON station.id_com = commune.insee_com
JOIN departement
    ON commune.id_dept = departement.id_departement
WHERE polluant.nom_poll = 'dioxyde d''azote'
GROUP BY "Période"
ORDER BY "Période"
;
```
Average values per period PM10
```SQL
SELECT AVG(releve.valeur) AS "PM10(µg/m³)",
        CASE
        WHEN CAST(releve.date_heure_releve AS DATE) < '2020-03-17' THEN '1 Avant le premier confinement'
        WHEN ('2020-03-17' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-05-11') THEN '2 Pendant le premier confinement'
        WHEN ('2020-05-11' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-10-30') THEN '3 Entre les deux confinements'
        WHEN ('2020-10-30' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-12-15') THEN '4 Pendant le deuxième confinement'
        WHEN '2020-12-15' <= CAST(releve.date_heure_releve AS DATE) THEN '5 Après le deuxième confinement'
        ELSE 'UNSET'
    END AS "Période"
FROM releve
JOIN polluant
    ON releve.id_pol_releve = polluant.id_polluant
JOIN station
    ON releve.id_station = station.id_station
JOIN commune
    ON station.id_com = commune.insee_com
JOIN departement
    ON commune.id_dept = departement.id_departement
WHERE polluant.nom_poll = 'particules PM10'
GROUP BY "Période"
ORDER BY "Période"
;
```
Average values per period for both pollutants
```SQL
SELECT AVG(releve.valeur) AS "PM10(µg/m³)",
        CASE
        WHEN CAST(releve.date_heure_releve AS DATE) < '2020-03-17' THEN '1 Avant le premier confinement'
        WHEN ('2020-03-17' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-05-11') THEN '2 Pendant le premier confinement'
        WHEN ('2020-05-11' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-10-30') THEN '3 Entre les deux confinements'
        WHEN ('2020-10-30' <= CAST(releve.date_heure_releve AS DATE)) AND (CAST(releve.date_heure_releve AS DATE) < '2020-12-15') THEN '4 Pendant le deuxième confinement'
        WHEN '2020-12-15' <= CAST(releve.date_heure_releve AS DATE) THEN '5 Après le deuxième confinement'
        ELSE 'UNSET'
    END AS "Période"
FROM releve
JOIN polluant
    ON releve.id_pol_releve = polluant.id_polluant
JOIN station
    ON releve.id_station = station.id_station
JOIN commune
    ON station.id_com = commune.insee_com
JOIN departement
    ON commune.id_dept = departement.id_departement
GROUP BY "Période"
ORDER BY "Période"
;
```
Average overall values for years from 2015 to 2020 for mixed pollutants pm10 & no2
```SQL
SELECT AVG(valeur) as "valeur moyenne",
    polluant.nom_poll as "Polluant",
    CASE WHEN (releve.metrique = 'annuelle') AND (releve.date_heure_releve > '2014-12-31') AND (releve.date_heure_releve < '2016-01-01') THEN '2015'
    WHEN (releve.metrique = 'annuelle') AND (releve.date_heure_releve > '2015-12-31') AND (releve.date_heure_releve < '2017-01-01') THEN '2016'
    WHEN (releve.metrique = 'annuelle') AND (releve.date_heure_releve > '2016-12-31') AND (releve.date_heure_releve < '2018-01-01') THEN '2017'
    WHEN (releve.metrique = 'annuelle') AND (releve.date_heure_releve > '2017-12-31') AND (releve.date_heure_releve < '2019-01-01') THEN '2018'
    WHEN (releve.metrique = 'annuelle') AND (releve.date_heure_releve > '2018-12-31') AND (releve.date_heure_releve < '2020-01-01') THEN '2019'
    WHEN (releve.metrique = 'journalier') AND (releve.date_heure_releve > '2019-12-31') AND (releve.date_heure_releve < '2021-01-01') THEN '2020'
    ELSE 'Relevés d''avant 2015 ou d''après 2020'
    END AS "Année"
FROM releve
JOIN polluant
    ON releve.id_pol_releve = polluant.id_polluant
JOIN station
    ON releve.id_station = station.id_station
JOIN commune
    ON station.id_com = commune.insee_com
JOIN departement
    ON commune.id_dept = departement.id_departement
WHERE ((releve.metrique = 'annuelle') OR (releve.metrique = 'journalier'))
    AND (releve.date_heure_releve > '2014-12-31')
    AND (releve.date_heure_releve < '2021-01-01')
GROUP BY "Année", "Polluant"
ORDER BY "Année" DESC
;
```
 
SELECT   COUNT(*) AS nbr_doublon, id_station, date_heure_releve, id_pol_releve, valeur, metrique
FROM releve 
WHERE metrique='horaire'
GROUP BY id_station, date_heure_releve, id_pol_releve, valeur, metrique
HAVING   COUNT(*) > 1;


DELETE
FROM releve
WHERE id_releve IN (
SELECT MAX(id_releve)
FROM   releve
WHERE metrique='horaire'
GROUP BY id_pol_releve, id_station, date_heure_releve, valeur, unite, metrique
HAVING COUNT(*) > 1)


cur.execute("""DELETE FROM releve

        WHERE id_releve IN (
        SELECT MAX(id_releve)
        FROM   releve
        WHERE metrique='horaire'
        GROUP BY id_pol_releve, id_station, date_heure_releve, valeur, unite, metrique
        HAVING COUNT(*) > 1) """)   


ur.execute("DROP TABLE IF EXISTS releve CASCADE;")
    cur.execute("DROP TABLE IF EXISTS departement CASCADE;")
    cur.execute("DROP TABLE IF EXISTS commune CASCADE;")
    cur.execute("DROP TABLE IF EXISTS polluant CASCADE;")
    cur.execute("DROP TABLE IF EXISTS station CASCADE;")


CREATE TABLE archive_releve (
    "id_releve" INT,
    "id_pol_releve" INT, 
    "id_station" INT,
    "date_heure_releve" TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY ("id_releve")
);

INSERT INTO archive_releve (")id_releve", "id_pol_releve", "
