#!/usr/bin/env python3
# -*- coding: utf8 -*-
import requests as rq
import json
import psycopg2
from psycopg2.extensions import parse_dsn
import shutil
import os
import pandas as pd
from pandas import DataFrame
import sys 

#os.remove("./.docker/data/polluant.csv")
'''
rpm10 = rq.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson')
rno2 = rq.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson')

datapm10 = rpm10.json()
datano2 = rno2.json()
'''
#--------------- Fonctions du script -----------------------------

def download_json_from_url(arg : str):
    print(f'Tentative de téléchargement depuis \"{arg}\"...')
    try:
        data = rq.get(arg).json() # get the json dict if possible
    except:
        data = 0 # else set dummy value
    if type(data) is dict: #Case: got json dict
        if list(data.keys())[0] == 'errors': #Check link integrity
            print(f'Lien invalide: {arg} - returning None')
            print(data['errors'])
            return None #Return none on failed download
        else: #Integrity check passed, returning data
            print('Téléchargement terminé.')
        return data # return the single json dict
    else: #Case: did not get json dict
        print(f'L\'URL \"{arg}\" ne retourne pas un dictionnaire,\n\
            annulation de l\'exécution de la fonction - returning None')
        return None

def json_to_csv(liste_dict):

    raw_data = {"longitude":[],"lattitude":[], "nom_dept":[], "nom_com":[], "insee_com":[], "nom_station":[], "code_station":[], "typologie":[], "influence":[], "nom_poll":[], "id_poll_ue":[], "valeur":[], "unite":[], "metrique":[], "date_debut":[], "date_fin":[], "x_wgs84":[], "y_wgs84":[], "x_reglementaire":[], "y_reglementaire":[]}

    for j in range(len(liste_dict)):
        for i in range(len(liste_dict[j]['features'])):
            raw_data["nom_dept"].append(liste_dict[j]['features'][i]["properties"]["nom_dept"])
            raw_data["nom_com"].append(liste_dict[j]["features"][i]["properties"]["nom_com"])
            raw_data["insee_com"].append(liste_dict[j]["features"][i]["properties"]["insee_com"])
            raw_data["nom_station"].append(liste_dict[j]["features"][i]["properties"]["nom_station"])
            raw_data["code_station"].append(liste_dict[j]["features"][i]["properties"]["code_station"])
            raw_data["typologie"].append(liste_dict[j]["features"][i]["properties"]["typologie"])
            raw_data["influence"].append(liste_dict[j]["features"][i]["properties"]["influence"])
            raw_data["nom_poll"].append(liste_dict[j]["features"][i]["properties"]["nom_poll"])
            #raw_data["polluant_court"].append(datapm10["features"][i]["properties"]["polluant_court"])
            raw_data["id_poll_ue"].append(liste_dict[j]["features"][i]["properties"]["id_poll_ue"])
            raw_data["valeur"].append(liste_dict[j]["features"][i]["properties"]["valeur"])
            raw_data["unite"].append(liste_dict[j]["features"][i]["properties"]["unite"])
            raw_data["metrique"].append(liste_dict[j]["features"][i]["properties"]["metrique"])
            raw_data["date_debut"].append(liste_dict[j]["features"][i]["properties"]["date_debut"])
            raw_data["date_fin"].append(liste_dict[j]["features"][i]["properties"]["date_fin"])
            #raw_data["validite"].append(datapm10["features"][i]["properties"]["validite"])
            raw_data["x_wgs84"].append(liste_dict[j]["features"][i]["properties"]["x_wgs84"])
            raw_data["y_wgs84"].append(liste_dict[j]["features"][i]["properties"]["y_wgs84"])
            raw_data["x_reglementaire"].append(liste_dict[j]["features"][i]["properties"]["x_reglementaire"])
            raw_data["y_reglementaire"].append(liste_dict[j]["features"][i]["properties"]["y_reglementaire"])
            raw_data["longitude"].append(liste_dict[j]["features"][i]["geometry"]["coordinates"][0])
            raw_data["lattitude"].append(liste_dict[j]["features"][i]["geometry"]["coordinates"][1])

    if os.path.isfile('/home/merouane/projet_2-groupe_2/.docker/data/polluant.csv'):
        df = pd.read_csv('/home/merouane/projet_2-groupe_2/.docker/data/polluant.csv')
        df2 = DataFrame(raw_data)
        df_final = df2.append(df, ignore_index=True)
        wt_db = df_final.drop_duplicates(keep='first')
        wt_db.to_csv("./.docker/data/polluant.csv", index=False)
    else:
        df = DataFrame(raw_data)
        df.to_csv("./.docker/data/polluant.csv", index=False)


def sql_tables():

    #shutil.move("polluant.csv", "./.docker/data/")
    db_dsn = "postgres://postgres:test@localhost:5432/projet2"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)

    cur = conn.cursor()

    
    cur.execute("DROP TABLE IF EXISTS raw_data CASCADE;")
    cur.execute("DROP TABLE IF EXISTS releve CASCADE;")
    cur.execute("DROP TABLE IF EXISTS departement CASCADE;")
    cur.execute("DROP TABLE IF EXISTS commune CASCADE;")
    cur.execute("DROP TABLE IF EXISTS polluant CASCADE;")
    cur.execute("DROP TABLE IF EXISTS station CASCADE;")
    cur.execute("DROP TABLE IF EXISTS archive_releve CASCADE;")

    cur.execute("""CREATE TABLE IF NOT EXISTS raw_data(
        "longitude" FLOAT,
        "lattitude" FLOAT,
        "nom_dept" TEXT,
        "nom_com" TEXT,
        "insee_com" INT,
        "nom_station" TEXT,
        "code_station" TEXT,
        "typologie" TEXT,
        "influence" TEXT,
        "nom_poll" TEXT,
        "id_poll_ue" INT,
        "valeur" FLOAT,
        "unite" TEXT,
        "metrique" TEXT,    
        "date_debut" TIMESTAMP WITH TIME ZONE,
        "date_fin" TIMESTAMP WITH TIME ZONE,
        "x_wgs84" FLOAT,
        "y_wgs84" FLOAT,
        "x_reglementaire" FLOAT,
        "y_reglementaire" FLOAT);""")

    cur.execute("COPY raw_data FROM '/data/polluant.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);")
    
    cur.execute("""CREATE TABLE IF NOT EXISTS departement (
        "id_departement" SERIAL,
        "nom_dept" TEXT,
        PRIMARY KEY ("id_departement"));""")

    cur.execute("""CREATE TABLE IF NOT EXISTS commune (
        "id_dept" INT, 
        "nom_com" TEXT,
        "insee_com" INT,
        PRIMARY KEY ("insee_com"),
        FOREIGN KEY ("id_dept") REFERENCES departement ("id_departement"));""")

    cur.execute("""CREATE TABLE IF NOT EXISTS station (
        "id_station" SERIAL,
        "code_station" TEXT,
        "id_com" INT,
        "nom_station" TEXT,
        "longitude" FLOAT,
        "lattitude" FLOAT,
        PRIMARY KEY ("id_station"),
        FOREIGN KEY ("id_com") REFERENCES commune ("insee_com"));""")
    
    cur.execute("""CREATE TABLE IF NOT EXISTS polluant (
        "id_polluant" SERIAL,
        "nom_poll" TEXT,
        PRIMARY KEY ("id_polluant"));""")
    
    cur.execute("""CREATE TABLE IF NOT EXISTS releve (
        "id_releve" SERIAL,
        "id_pol_releve" INT, 
        "id_station" INT,
        "date_heure_releve" TIMESTAMP WITH TIME ZONE,
        "valeur" FLOAT,
        "unite" TEXT,
        "metrique" TEXT,
        PRIMARY KEY ("id_releve"),
        FOREIGN KEY ("id_pol_releve") REFERENCES polluant ("id_polluant"),
        FOREIGN KEY ("id_station") REFERENCES station ("id_station"));""")


    conn.commit()
    
    cur.close()
    conn.close()

def load_sql_tables():
    db_dsn = "postgres://postgres:test@localhost:5432/projet2"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)

    cur = conn.cursor()

    cur.execute("""INSERT INTO departement ("nom_dept")
        SELECT DISTINCT(nom_dept)
        FROM raw_data""")


    cur.execute("""INSERT INTO commune ("nom_com", "insee_com", "id_dept") 
        SELECT DISTINCT(raw_data.nom_com), raw_data.insee_com, departement.id_departement 
        FROM raw_data 
        JOIN departement 
        ON departement.nom_dept = raw_data.nom_dept; """)

    

    cur.execute("""INSERT INTO station ("nom_station", "longitude", "lattitude", "code_station", "id_com") 
        SELECT DISTINCT(raw_data.nom_station), raw_data.longitude, raw_data.lattitude, raw_data.code_station, commune.insee_com 
        FROM raw_data 
        JOIN commune 
        ON commune.insee_com = raw_data.insee_com; """)

    

    cur.execute("""INSERT INTO polluant ("nom_poll") 
        SELECT DISTINCT(nom_poll)
        FROM raw_data;""")



    cur.execute("""INSERT INTO releve ("id_pol_releve", "id_station", "date_heure_releve", "valeur", "unite", "metrique") 
        SELECT polluant.id_polluant, station.id_station, raw_data.date_debut, raw_data.valeur, raw_data.unite, raw_data.metrique
        FROM raw_data
        JOIN polluant
        ON polluant.nom_poll = raw_data.nom_poll
        JOIN station
        ON  station.nom_station = raw_data.nom_station;""")

    


    conn.commit()
    
    cur.close()
    conn.close()
#-----------------------------------------------------------------
#--------------- Collectes des données ---------------------------

#--------- Lien API ----------------------------

r = rq.get("https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson") #PM10 horraire
r2 = rq.get("https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson")#NO3 horaire               
r3 = rq.get("https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_4.geojson")#PM10 journaliere
r4 = rq.get("https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_1.geojson")#NO2 journaliere

#--------- convertion dictionnaire -------------
data = r.json() 
data2= r2.json()
data3 = r3.json()
data4= r4.json()

#--------------- Convertion du fichier Json en CSV ---------------

liste_dict = [data ,data2, data3, data4] 

json_to_csv(liste_dict)

#----------------------------------------------------------------- 
#--------------- Creation des table ------------------------------

sql_tables()

#-----------------------------------------------------------------
#-------------- Allimentation des tables -------------------------

load_sql_tables()

#-----------------------------------------------------------------


#os.remove("./.docker/data/polluant.csv")